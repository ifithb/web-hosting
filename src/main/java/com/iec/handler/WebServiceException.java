/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.handler;

import java.io.Serializable;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.iec.model.ErrorMessage;

/**
 *
 * @author Arnold
 */
public class WebServiceException extends WebApplicationException
implements Serializable {

    private static final long serialVersionUID = 1169426381288170661L;

//    public WebServiceException() {
//        super();
//    }

    public WebServiceException(String msg) {
//        super(msg);
    	super(Response.status(Status.BAD_REQUEST )
                .entity(new ErrorMessage(msg))
                .type(MediaType.APPLICATION_JSON).build());
    }

//    public WebServiceException(String msg, Exception e) {
//        super(msg, e);
//    }
}
