/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.handler;

import com.iec.model.ErrorMessage;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Ga kepake
 * 
 * @author Arnold
 */
@Provider
public class WebServiceExceptionHandler implements ExceptionMapper<WebServiceException> {

	@Override
    public Response toResponse(final WebServiceException exception) {
        return Response.status(Status.BAD_REQUEST )
                .entity(new ErrorMessage(exception.getMessage()))
                .type(MediaType.APPLICATION_JSON).build();
    }
}
