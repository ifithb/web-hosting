/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.SubscribeDao;
import com.iec.model.Subscribe;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Arnold
 */
@Path("/subscribes")
public class SubscribeResource {

    @Path("/show/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") Long id) {
        SubscribeDao sd = new SubscribeDao();
        Subscribe subscribe = sd.findOne(id);

        return Response.ok(subscribe).build();
    }

    @Path("/getall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        SubscribeDao sd = new SubscribeDao();
        List<Subscribe> subscribes = sd.findAll();
        return Response.ok(subscribes).build();
    }
}
