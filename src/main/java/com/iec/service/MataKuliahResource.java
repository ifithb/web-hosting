/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iec.service;

import com.iec.dao.MataKuliahDao;
import com.iec.model.MataKuliah;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Kushrenada
 */
@Path("/matakuliah")
public class MataKuliahResource {
    
    @Path("/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") int id) {
        MataKuliahDao md = new MataKuliahDao();
        MataKuliah mk = md.findOne(id);

        return Response.ok(mk)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    @Path("")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        MataKuliahDao md = new MataKuliahDao();
        List<MataKuliah> mks = md.findAll();
        return Response.ok(mks)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    
}
