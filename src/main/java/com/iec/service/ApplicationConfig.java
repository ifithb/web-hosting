/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.iec.service;

import java.util.Set;

/**
 *
 * @author Kushrenada
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends javax.ws.rs.core.Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
//        resources.add(com.iec.handler.WebServiceExceptionHandler.class);
        resources.add(com.iec.handler.WebServiceExceptionHandler.class);
        resources.add(com.iec.service.CommentResource.class);
        resources.add(com.iec.service.DosenMKResource.class);
        // coba
        resources.add(com.iec.service.EnrollResource.class);
        resources.add(com.iec.service.HelloResource.class);
        resources.add(com.iec.service.KelasResource.class);
        resources.add(com.iec.service.MataKuliahResource.class);
        resources.add(com.iec.service.PengumumanResource.class);
        resources.add(com.iec.service.SubscribeResource.class);
        resources.add(com.iec.service.UserResource.class);
    }

}
