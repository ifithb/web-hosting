package com.iec.service;

import com.iec.dao.UserDao;
import com.iec.dto.UserDTO;
import com.iec.handler.WebServiceException;
import com.iec.model.User;

import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author Kushrenada
 */
@Path("/users")
public class UserResource {

    UserDao ud = new UserDao();
    User user;

    @Path("/show/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") Integer id) {
        user = ud.findOne(id);

        return Response.ok(user)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/getall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<User> users = ud.findAll();
//        for (User user : users) {
//            this.user.setPassword("");
//        }
        return Response.ok(users).build();
    }

    @Path("/login")
    @POST
//    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response login(User user) {
        User dariDB = ud.login(user.getUsername(), user.getPassword());
        dariDB.setPassword("");
        return Response.status(200).entity(dariDB).build();
    }

    @Path("/register")
    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(User user, @Context UriInfo uriInfo) {
        ud.save(user);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(user.getId()));
        if (user == null) {
            throw new WebServiceException("registration failed");
        }
        return Response.created(builder.build())
                .status(200)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600").allow("OPTIONS")
                .build();
    }

    @Path("/update/{id}")
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response update(User user) {
        ud.update(user);
        return Response.ok(user).status(200).build();
    }

    @Path("/delete/{id}")
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        User user = ud.findOne(id);
        if (user != null) {
            ud.delete(user);

            return Response.ok(user)
                    .status(200)
                    //                    .header("Access-Control-Allow-Origin", "*")
                    //                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    //                    .header("Access-Control-Allow-Credentials", "true")
                    //                    .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                    //                    .header("Access-Control-Max-Age", "1209600").allow("OPTIONS")
                    .build();
        } else {
            return Response.status(422).build(); // unprocessable entity
        }
    }

    @Path("/getname/{name}")
    @GET
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response showByName(@PathParam("name") final String name) throws WebServiceException {
        user = ud.findByName(name);
        if (user == null) {
            throw new WebServiceException("User does not exist with name: " + name);
        }
        return Response.ok().entity(user).build();
    }

    @Path("/getusername/{username}")
    @GET
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response showByUsername(@PathParam("username") final String username) throws WebServiceException {
        user = ud.findByUsername(username);

        // http://stackoverflow.com/questions/583973/jax-rs-jersey-how-to-customize-error-handling
        // pake @Provider di com.iec.handler.WebServiceExceptionHandler tapi ga bisa
        // jadi kelas com.iec.handler.WebServiceExceptionHandler bisa dihapus harusnya
        if (user == null) {
            throw new WebServiceException("User does not exist with username: " + username);
        }

        return Response.ok().entity(user).build();
    }

//    @Path("/checkpass")
//    @POST
//    @Produces(value = MediaType.APPLICATION_JSON)
//    public Response checkPassword(@PathParam("username") String username, @PathParam("password") String password) {
//        User dariDB;
//        dariDB = ud.checkUser(user.getUsername(), user.getPassword());
//        if (user == null) {
//            throw new WebServiceException("Username and password combination does not match, please try again");
//        }
//        dariDB.setPassword("");
//        return Response.status(200).entity(dariDB).build();
//    }
    @Path("/checkpass")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response checkPassword(User user) {
        User dariDB;
        dariDB = ud.checkUser(user.getUsername(), user.getPassword());
        if (dariDB == null) {
            throw new WebServiceException("Username and password combination does not match, please try again");
        }
        dariDB.setPassword("");
        return Response.status(200).entity(dariDB).build();
    }

    @Path("/changepass/{username}")
    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response changePassword(@PathParam("username") String username, UserDTO userDTO) {
        User user = ud.changePass(userDTO);
        
        return Response.ok(user).status(200).build();
    }
}
