/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.PengumumanDao;
import com.iec.model.Pengumuman;

import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author Kushrenada
 */
@Path("/pengumumans")
public class PengumumanResource {

    @Path("/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") long id) {
        PengumumanDao pd = new PengumumanDao();
        Pengumuman pengumuman = pd.findOne(id);

        return Response.ok(pengumuman)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    @Path("/getbyposter/{username}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getByPoster(@PathParam("username") String username) {
        PengumumanDao pd = new PengumumanDao();
        List<Pengumuman> pengumumans = pd.findByPoster(username);
        return Response.ok(pengumumans)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    @Path("/getbykelas")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getByKelas() {
        PengumumanDao pd = new PengumumanDao();
        List<Pengumuman> pengumumans = pd.findByKelas();
        return Response.ok(pengumumans)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    
    @Path("/getall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        PengumumanDao pd = new PengumumanDao();
        List<Pengumuman> pengumumans = pd.findAll();
        return Response.ok(pengumumans)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
    @Path("/getallactive")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAllActive() {
        PengumumanDao pd = new PengumumanDao();
        List<Pengumuman> pengumumans = pd.findAllActive();
        return Response.ok(pengumumans)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/create")
    @POST
    //@Consumes(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(Pengumuman pengumuman, @Context UriInfo uriInfo) {
        PengumumanDao pd = new PengumumanDao();
        pd.save(pengumuman);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(pengumuman.getId()));
        return Response.created(builder.build())
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/update/{id}")
    @PUT
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response update(Pengumuman pengumuman) {
        PengumumanDao pd = new PengumumanDao();
        pd.update(pengumuman);
        return Response.ok(pengumuman)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/delete/{id}")
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        PengumumanDao pd = new PengumumanDao();
        Pengumuman pengumuman = pd.findOne(id);
        if (pengumuman != null) {
            pd.delete(pengumuman);

            return Response.ok(pengumuman)
                    //                    .header("Access-Control-Allow-Origin", "*")
                    //                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    //                    .header("Access-Control-Allow-Credentials", "true")
                    //                    .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                    //                    .header("Access-Control-Max-Age", "1209600")
                    .build();
        } else {
            return Response.status(422).build(); // unprocessable entity
        }
    }
}
