/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.CommentDao;
import com.iec.handler.WebServiceException;
import com.iec.model.Comment;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author Kushrenada
 */
@Path("/comments")
public class CommentResource {

    CommentDao cd = new CommentDao();
    Comment comment;

    @Path("/show/{id}")
    @GET
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") Long id) {
        comment = cd.findOne(id);
        return Response.ok(comment).build();
    }

    @Path("/showcommnetbytitle/{title}")
    @GET
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response showByTitle(@PathParam("title") final String title) throws WebServiceException {
        List<Comment> comments = cd.getCommentByPengumumanTitle(title);
        if (comment == null) {
            throw new WebServiceException("Pengumuman does not exist with title: " + title);
        }
        return Response.ok().entity(comment).build();
    }

    @Path("/showcommentbypengid/{id}")
    @GET
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response showByPengId(@PathParam("id") final long id) throws WebServiceException {
        List<Comment> comments = cd.getCommentByPengumumanId(id);
        if (comments == null) {
            throw new WebServiceException("Pengumuman does not exist with id: " + id);
        }
        return Response.ok().entity(comments).build();
    }

    @Path("/showall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response showAll(Comment comment) {
        List<Comment> comments = cd.findAll();
        return Response.ok(comments).build();
    }

    @Path("/create")
    @POST
    //@Consumes(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(Comment comment, @Context UriInfo uriInfo) {
        CommentDao cd = new CommentDao();
        cd.save(comment);
        if (comment == null) {
            throw new WebServiceException("failed to insert comment");
        } else {
            UriBuilder builder = uriInfo.getAbsolutePathBuilder();
            builder.path(Long.toString(comment.getId()));
            return Response.created(builder.build()).status(200).build();
        }
//        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
//        builder.path(Long.toString(comment.getId()));
//
//        return Response.created(builder.build()).status(200).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response update(Comment comment) {
        CommentDao cd = new CommentDao();
        cd.update(comment);
        return Response.ok(comment)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        CommentDao cd = new CommentDao();
        Comment comment = cd.findOne(id);
        if (comment != null) {
            cd.delete(comment);

            return Response.ok(comment)
                    //                    .header("Access-Control-Allow-Origin", "*")
                    //                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    //                    .header("Access-Control-Allow-Credentials", "true")
                    //                    .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                    //                    .header("Access-Control-Max-Age", "1209600")
                    .build();
        } else {
            return Response.status(422).build(); // unprocessable entity
        }
    }
}
