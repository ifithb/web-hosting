package com.iec.service;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/hello")
public class HelloResource {
    @Path("")
    @GET
    public Response hello() {
        return Response.ok("Hello, World!").build();
    }
}
