/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.EnrollDao;
import com.iec.model.Enroll;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author Arnold
 */
@Path("/enrolls")
public class EnrollResource {

    EnrollDao ed;
    List<Enroll> enrolls;

    @Path("/getall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        ed = new EnrollDao();
        enrolls = ed.findAll();
        return Response.ok(enrolls)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/create")
    @POST
    //@Consumes(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(Enroll enroll, @Context UriInfo uriInfo) {
        ed = new EnrollDao();
        ed.enroll(enroll);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Long.toString(enroll.getId()));
        return Response.created(builder.build())
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }
}
