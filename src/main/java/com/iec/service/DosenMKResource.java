/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.DosenMKDao;
import com.iec.model.DosenMK;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author anther
 */
@Path("/dmk")
public class DosenMKResource {

    DosenMKDao dmkd;
    List<DosenMK> dmks;

    @Path("/show/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") long id) {
        dmkd = new DosenMKDao();
        DosenMK dosenMK = dmkd.findOne(id);

        return Response.ok(dosenMK)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/getall")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        dmkd = new DosenMKDao();
        dmks = dmkd.findAll();
        return Response.ok(dmks)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/update/{id}")
    @PUT
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response update(DosenMK dosenMK) {
        dmkd = new DosenMKDao();
        dmkd.update(dosenMK);
        return Response.ok(dosenMK)
                //                .header("Access-Control-Allow-Origin", "*")
                //                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                //                .header("Access-Control-Allow-Credentials", "true")
                //                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                //                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/delete/{id}")
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") long id) {
        dmkd = new DosenMKDao();
        DosenMK dosenMK = dmkd.findOne(id);
        if (dosenMK != null) {
            dmkd.delete(dosenMK);

            return Response.ok(dosenMK)
                    //                    .header("Access-Control-Allow-Origin", "*")
                    //                    .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                    //                    .header("Access-Control-Allow-Credentials", "true")
                    //                    .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                    //                    .header("Access-Control-Max-Age", "1209600")
                    .build();
        } else {
            return Response.status(422).build(); // unprocessable entity
        }
    }
}
