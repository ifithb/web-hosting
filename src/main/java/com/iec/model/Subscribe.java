/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Arnold
 */
@Entity
//@Table(name = "subscribes", catalog = "iec_announcement")
@Table(name = "subscribes", catalog = "backup_iecannouncement")
@AssociationOverrides({
    @AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
    @AssociationOverride(name = "pk.kelas", joinColumns = @JoinColumn(name = "id_kelas"))
})
public class Subscribe implements Serializable {

    private static final long serialVersionUID = 7L;

    @EmbeddedId
    @JsonIgnore
    private SubscribeId pk;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.pk);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Subscribe other = (Subscribe) obj;
        if (!Objects.equals(this.pk, other.pk)) {
            return false;
        }
        return true;
    }

    public Subscribe() {
    }

    public SubscribeId getPk() {
        return pk;
    }

    public void setPk(SubscribeId pk) {
        this.pk = pk;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public User getUser() {
        return pk != null ? pk.getUser() : null;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_kelas")
    public Kelas getKelas() {
        return pk != null ? pk.getKelas() : null;
    }
}
