/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 *
 * @author Arnold
 */
@Embeddable
public class SubscribeId implements Serializable {

    private static final long serialVersionUID = 7L;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Kelas kelas;

    public SubscribeId() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + Objects.hashCode(this.kelas);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this != o || getClass() != o.getClass()) {
            return false;
        }

        SubscribeId other = (SubscribeId) o;

        // Cek order
        if (user == null) {
            if (other.user != null) {
                return false;
            }
        } else if (other.user == null) {
            return false;
        } else if (!user.getId().equals(other.user.getId())) {
            return false;
        }

        // Cek item
        if (kelas == null) {
            if (other.kelas != null) {
                return false;
            }
        } else if (other.kelas == null) {
            return false;
        } //            else if (!kelas.getId().equals(other.kelas.getId())) {
        //                return false;
        //            }

        return true;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Kelas getKelas() {
        return kelas;
    }

    public void setKelas(Kelas kelas) {
        this.kelas = kelas;
    }
}
