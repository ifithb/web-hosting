/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

/**
 *
 * @author anther
 */
import java.io.Serializable;
import javax.persistence.*;

@Entity
//@Table(name = "dosenmk", catalog = "iec_announcement")
@Table(name = "dosenmk", catalog = "backup_iecannouncement")
public class DosenMK implements Serializable {

    private static final long serialVersionUID = 7L;

    private Long id;
    private User user;
    private MataKuliah matkul;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mk_id")
    public MataKuliah getMatkul() {
        return matkul;
    }

    public void setMatkul(MataKuliah matkul) {
        this.matkul = matkul;
    }
}