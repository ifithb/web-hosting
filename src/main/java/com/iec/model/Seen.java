/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Kushrenada
 */
@Entity
//@Table(name = "seens", catalog = "iec_announcement")
@Table(name = "seens", catalog = "backup_iecannouncement")
@AssociationOverrides({
    @AssociationOverride(name = "pk.user", joinColumns = @JoinColumn(name = "user_id")),
    @AssociationOverride(name = "pk.pengumuman", joinColumns = @JoinColumn(name = "peng_id"))
})
public class Seen implements Serializable {
    
    private static final long serialVersionUID = 7L;
    
    @EmbeddedId
    @JsonIgnore
    private SeenId pk = new SeenId();
    
    public Seen() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.pk);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seen other = (Seen) obj;
        if (!Objects.equals(this.pk, other.pk)) {
            return false;
        }
        return true;
    }

    public SeenId getPk() {
        return pk;
    }

    public void setPk(SeenId pk) {
        this.pk = pk;
    }
}
