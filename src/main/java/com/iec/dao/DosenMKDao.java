/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.DosenMK;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author anther
 */
public class DosenMKDao {

    public List<DosenMK> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<DosenMK> dmks = null;
        try {
            Query query = session.createQuery("select d from DosenMK d "
                    + "join fetch d.matkul "
                    + "join fetch d.user");
            dmks = query.list();
            for (DosenMK dmk : dmks) {
                dmk.getMatkul().setDosen(null);
                dmk.getUser().setPengumumans(null);
                dmk.getUser().setMataKuliahs(null);
                dmk.getUser().setSeens(null);
                dmk.getUser().setComments(null);
                dmk.getUser().setPassword("");
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return dmks;
    }

    public DosenMK save(DosenMK dosenMK) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(dosenMK);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return dosenMK;
    }

    public DosenMK update(DosenMK dosenMK) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(dosenMK);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return dosenMK;
    }

    public DosenMK delete(DosenMK dosenMK) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(dosenMK);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return dosenMK;
    }

    public DosenMK findOne(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        DosenMK dosenMK = null;

        try {
            dosenMK = (DosenMK) session.get(DosenMK.class, id);
            dosenMK.getMatkul().setDosen(null);
            dosenMK.getUser().setMataKuliahs(null);
            dosenMK.getUser().setComments(null);
            dosenMK.getUser().setPengumumans(null);
            dosenMK.getUser().setPassword("");
            dosenMK.getUser().setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return dosenMK;
    }
}
