/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Pengumuman;
import java.util.List;
import org.hibernate.*;

/**
 *
 * @author Kushrenada
 */
public class PengumumanDao {

    public Pengumuman findOne(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Pengumuman pengumuman = null;

        try {
            pengumuman = (Pengumuman) session.get(Pengumuman.class, id);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return pengumuman;
    }

    public List<Pengumuman> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Pengumuman> pengumumans = null;

        try {
            Query query = session.createQuery("select p from Pengumuman p "
                    + "join fetch p.user "
                    + "join fetch p.kelas k join fetch k.mk ");
            pengumumans = query.list();
            for (Pengumuman pengumuman : pengumumans) {
                pengumuman.setSeens(null);
                pengumuman.setComments(null);
                pengumuman.getKelas().getMk().setDosen(null);
                pengumuman.getUser().setPassword("");
                pengumuman.getUser().setComments(null);
                pengumuman.getUser().setMataKuliahs(null);
                pengumuman.getUser().setPengumumans(null);
                pengumuman.getUser().setSeens(null);

            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return pengumumans;
    }

    public List<Pengumuman> findAllActive() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Pengumuman> pengumumans = null;
        try {
            Query query = session.createQuery("select p from Pengumuman p "
                    + "join fetch p.user "
                    + "join fetch p.kelas k join fetch k.mk "
                    + "where p.expiredOn >= CURRENT_TIMESTAMP()");
            pengumumans = query.list();
            for (Pengumuman pengumuman : pengumumans) {
                pengumuman.setSeens(null);
                pengumuman.setComments(null);
                pengumuman.getKelas().getMk().setDosen(null);
                pengumuman.getUser().setPassword("");
                pengumuman.getUser().setComments(null);
                pengumuman.getUser().setMataKuliahs(null);
                pengumuman.getUser().setPengumumans(null);
                pengumuman.getUser().setSeens(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return pengumumans;
    }

    public List<Pengumuman> findByPoster(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Pengumuman> pengumumans = null;

        try {
            Query query = session.createQuery("from Pengumuman p join fetch p.user u"
                    + " join fetch p.kelas k join fetch k.mk"
                    + " where u.username = :username");
            query.setParameter("username", username);
            pengumumans = query.list();
            for (Pengumuman pengumuman : pengumumans) {
                if (pengumuman.getUser() != null) {
                    pengumuman.getUser().setPassword("");
                    pengumuman.getUser().setPengumumans(null);
                    pengumuman.getUser().setMataKuliahs(null);
                    pengumuman.getUser().setSeens(null);
                    pengumuman.getUser().setComments(null);
                }
                pengumuman.getKelas().getMk().setDosen(null);
                pengumuman.setSeens(null);
                pengumuman.setComments(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return pengumumans;
    }

    public List<Pengumuman> findByKelas() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Pengumuman> pengumumans = null;

        try {
            Query query = session.createQuery("from Pengumuman p join fetch p.user u join fetch p.kelas k join fetch p.comment c");
            pengumumans = query.list();
            for (Pengumuman pengumuman : pengumumans) {
                if (pengumuman.getUser() != null && pengumuman.getKelas() != null && pengumuman.getComments() != null) {
                    pengumuman.getUser().setPassword("");
                    pengumuman.getUser().setPengumumans(null);
                    pengumuman.getUser().setMataKuliahs(null);
                    pengumuman.getUser().setSeens(null);
                    pengumuman.getUser().setComments(null);
                    pengumuman.getKelas().setMk(null);
                }
                pengumuman.setSeens(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return pengumumans;
    }

    public Pengumuman save(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return pengumuman;
    }

    public Pengumuman update(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return pengumuman;
    }

    public Pengumuman delete(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return pengumuman;
    }
}
