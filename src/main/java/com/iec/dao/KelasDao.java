/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Kelas;
import java.util.List;
import org.hibernate.*;

/**
 *
 * @author Kushrenada
 */
public class KelasDao {

    public Kelas findOne(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Kelas kelas = null;
        try {
            kelas = (Kelas) session.get(Kelas.class, id);
            kelas.setMk(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return kelas;
    }

    public List<Kelas> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Kelas> kelases = null;
        try {
            Query query = session.createQuery("from Kelas k join fetch k.mk mk join fetch mk.dosen");
            kelases = query.list();
            for (Kelas kelas : kelases) {
                kelas.getMk().getDosen().setPassword("");
                kelas.getMk().getDosen().setComments(null);
                kelas.getMk().getDosen().setMataKuliahs(null);
                kelas.getMk().getDosen().setPengumumans(null);
                kelas.getMk().getDosen().setSeens(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return kelases;
    }

    public Kelas save(Kelas kelas) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(kelas);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return kelas;
    }
}
