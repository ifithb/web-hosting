/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Enroll;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Arnold
 */
public class EnrollDao {

    public Enroll enroll(Enroll enroll) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(enroll);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();

        }
        return enroll;
    }

    public List<Enroll> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Enroll> enrolls = null;
        try {
            Query query = session.createQuery("select e from Enroll e "
                    + "join fetch e.mk "
                    + "join fetch e.user");
            enrolls = query.list();
            for (Enroll enroll : enrolls) {
                enroll.getMk().setDosen(null);
                enroll.getUser().setPengumumans(null);
                enroll.getUser().setMataKuliahs(null);
                enroll.getUser().setSeens(null);
                enroll.getUser().setComments(null);
                enroll.getUser().setPassword("");
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return enrolls;
    }
}
