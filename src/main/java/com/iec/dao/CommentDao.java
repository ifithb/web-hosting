/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Comment;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Kushrenada
 */
public class CommentDao {

    public Comment findOne(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Comment comment = null;

        try {
            comment = (Comment) session.get(Comment.class, id);
            comment.setPengumuman(null);
            comment.setUser(null);

        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return comment;
    }

    public List<Comment> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Comment> comments = null;

        try {
            
            Query query = session.createQuery("from Comment");
            comments = query.list();

            for (Comment comment : comments) {
                comment.setPengumuman(null);
                comment.setUser(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();

        }
        return comments;
    }

    public List<Comment> getCommentByPengumumanId(long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Comment> comments = null;
        try {
            Query query = session.createQuery("from Comment c "
                    + "join fetch c.pengumuman p "
                    + "join fetch c.user u "
                    + "where p.id = :id");
            query.setParameter("id", id);
            comments = query.list();

            for (Comment comment : comments) { 
                if (comment.getPengumuman() != null) {
                    comment.getPengumuman().setComments(null);
                    comment.getPengumuman().setSeens(null);
                    comment.getPengumuman().setKelas(null);
                    comment.getPengumuman().setUser(null);
                }
                if (comment.getUser()!=null){
                    comment.getUser().setPassword("");
                    comment.getUser().setMataKuliahs(null);
                    comment.getUser().setComments(null);
                    comment.getUser().setSeens(null);
                    comment.getUser().setPengumumans(null);
                }
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return comments;
    }

    public List<Comment> getCommentByPengumumanTitle(String title) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Comment> comments = null;
        try {
            Query query = session.createQuery("select c from Comment c "
                    + "join fetch pengumuman p "
                    + "where p.judul like :title");
            query.setParameter("title", "%" + title + "%");
            comments = query.list();

            for (Comment comment : comments) {
                if (comment.getPengumuman() != null) {
                    comment.getPengumuman().setComments(null);
                    comment.getPengumuman().setSeens(null);
                    comment.getPengumuman().setKelas(null);
                    comment.getPengumuman().setUser(null);
                }
                comment.setUser(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return comments;
    }

    public Comment save(Comment comment) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(comment);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return comment;
    }

    public Comment update(Comment comment) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(comment);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return comment;
    }

    public Comment delete(Comment comment) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(comment);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return comment;
    }
}
