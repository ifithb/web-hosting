/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.MataKuliah;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Kushrenada
 */
public class MataKuliahDao {

    public MataKuliah findOne(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        MataKuliah mk = null;
        try {
            mk = (MataKuliah) session.get(MataKuliah.class, id);
            mk.setDosen(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return mk;
    }

    public List<MataKuliah> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<MataKuliah> mks = null;
        try {
            Query query = session.createQuery("from User");
            mks = query.list();
            for (MataKuliah mk : mks) {
                mk.setDosen(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return mks;
    }
    public MataKuliah save(MataKuliah mataKuliah) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(mataKuliah);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return mataKuliah;
    }

    public MataKuliah update(MataKuliah mataKuliah) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(mataKuliah);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return mataKuliah;
    }

    public MataKuliah delete(MataKuliah mataKuliah) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(mataKuliah);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return mataKuliah;
    }
}
