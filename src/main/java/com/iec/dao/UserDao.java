/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.dto.UserDTO;
import com.iec.handler.WebServiceException;
import com.iec.hibernate.HibernateUtil;
import com.iec.model.User;

import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.*;
import org.hibernate.service.spi.ServiceException;

/**
 *
 * @author Kushrenada
 */
public class UserDao {

    public User findOne(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {

            user = (User) session.get(User.class, id);
            if (user.getId() != null) {
                throw new ServiceException("Error: user not found for id: " + id);
            }
            user.setPassword("");
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public User findByName(String name) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;

        try {
            Query query = session.createQuery("from User where name like :name");
            query.setParameter("name", "%" + name + "%");
            user = (User) query.uniqueResult();
            user.setPassword("");
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);

        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public User login(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Query query = session.createQuery("from User u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            user = (User) query.uniqueResult();
            if (user == null) {
                return null;
            }
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public User findByUsername(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;

        try {
            Query query = session.createQuery("from User u where u.username = :username");
            query.setParameter("username", username);

            user = (User) query.uniqueResult();

            if (user == null) {
                return null;
            }

            user.setPassword("");
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);

        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public List<User> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<User> users = null;

        try {
            Query query = session.createQuery("from User");
            users = query.list();

            for (User user : users) {
                user.setComments(null);
                user.setPengumumans(null);
                user.setMataKuliahs(null);
                user.setSeens(null);
                user.setPassword("");
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return users;
    }

    public User save(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return user;
    }

    public User update(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user;
    }

    public User delete(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user;
    }

    public User checkUser(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;
        try {
            Query query = session.createQuery("from User u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            user = (User) query.uniqueResult();
            if (user == null) {
                return null;
            }
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public User changePass(UserDTO userDTO) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;
        try {
            Query q1 = session.createQuery("from User u where u.username = :username and u.password = :password");
            q1.setParameter("username", userDTO.getUsername());
            q1.setParameter("password", userDTO.getPassword());
            user = (User) q1.uniqueResult();
            if (user == null) {
                throw new WebServiceException("Username and password combination does not match, please try again");
            } else if (userDTO.getNewPassword() == null
                    || !userDTO.getNewPassword().equals(userDTO.getConfirmPassword())) {
                throw new WebServiceException("Your password and confirmation didn't match");
            }
            Query q2 = session.createQuery("update User set password = :password where username = :username");
            q2.setParameter("username", userDTO.getUsername());
            q2.setParameter("password", userDTO.getNewPassword());
            tx = session.beginTransaction();

            q2.executeUpdate(); 
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        user.setComments(null);
        user.setMataKuliahs(null);
        user.setPengumumans(null);
        user.setSeens(null);
        return user;
    }
}
