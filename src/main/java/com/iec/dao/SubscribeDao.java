/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Kelas;
import com.iec.model.Pengumuman;
import com.iec.model.Subscribe;
import com.iec.model.User;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Arnold
 */
public class SubscribeDao {
    
    public Subscribe findOne(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Subscribe subscribe = null;
        
        try {
            subscribe = (Subscribe) session.get(Subscribe.class, id);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
//            HibernateUtil.shutdown();
        }
        return subscribe;
    }
    
    @SuppressWarnings("unchecked")
    public List<Subscribe> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Subscribe> subscribes = null;
        try {
            Query query = session.createQuery("from Subscribe s join fetch s.user u join fetch s.kelas k");
            subscribes = (List<Subscribe>) query.list();
            
            for (Subscribe subscribe : subscribes) {
                User user = subscribe.getUser();
                if (user != null) {
                    user.setPassword("");
                    user.setComments(null);
                    user.setMataKuliahs(null);
                    user.setPengumumans(null);
                    user.setSeens(null);
                }                
                Kelas kelas = subscribe.getKelas();
                if (kelas != null) {
                    kelas.setMk(null);
                }
            }
            System.out.println(subscribes.size());
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return subscribes;
    }
}
