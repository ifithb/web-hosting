-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: 127.10.167.2:3306
-- Generation Time: May 16, 2016 at 01:43 PM
-- Server version: 5.5.45
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iec_announcement`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(10) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `user_id` int(10) NOT NULL,
  `peng_id` int(10) NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `user_id` (`user_id`,`peng_id`),
  KEY `peng_id` (`peng_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `comment`, `user_id`, `peng_id`) VALUES
(1, 'tugas nya perorangan?', 3, 1),
(5, 'tugas dikerjakan per-2 orang', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dosenmk`
--

CREATE TABLE IF NOT EXISTS `dosenmk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dosen_id` int(11) NOT NULL,
  `mk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dosen_id` (`dosen_id`),
  KEY `dosen_id_2` (`dosen_id`),
  KEY `mk_id` (`mk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(10) NOT NULL AUTO_INCREMENT,
  `kelas` char(10) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `semester_MK` int(10) NOT NULL,
  `id_mk` int(10) NOT NULL,
  PRIMARY KEY (`id_kelas`),
  KEY `id_mk` (`id_mk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`, `semester`, `semester_MK`, `id_mk`) VALUES
(1, 'a', 'ganjil', 1, 1),
(2, 'b', 'ganjil', 1, 1),
(3, 'a', 'ganjil', 1, 2),
(4, 'b', 'ganjil', 1, 2),
(5, 'a', 'ganjil', 3, 3),
(6, 'b', 'ganjil', 3, 3),
(7, 'a', 'ganjil', 1, 5),
(8, 'a', 'ganjil', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE IF NOT EXISTS `matakuliah` (
  `id_mk` int(10) NOT NULL AUTO_INCREMENT,
  `kode_mk` varchar(50) NOT NULL,
  `nama_mk` varchar(50) NOT NULL,
  `sks` int(10) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  PRIMARY KEY (`id_mk`),
  KEY `dosen_id` (`dosen_id`),
  KEY `dosen_id_2` (`dosen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`id_mk`, `kode_mk`, `nama_mk`, `sks`, `dosen_id`) VALUES
(1, 'IF-1', 'web desain', 2, 2),
(2, 'IF-2', 'algoritma', 3, 4),
(3, 'IF-3', 'basis data', 4, 4),
(4, 'TI-1', 'Kalkulus', 2, 6),
(5, 'KU-1', 'Core Skill', 0, 6),
(6, 'Dept-IF', 'Departemen', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengumumans`
--

CREATE TABLE IF NOT EXISTS `pengumumans` (
  `peng_id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `tipe` varchar(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  PRIMARY KEY (`peng_id`),
  KEY `user_id` (`user_id`,`id_kelas`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `pengumumans`
--

INSERT INTO `pengumumans` (`peng_id`, `judul`, `deskripsi`, `tipe`, `timestamp`, `user_id`, `id_kelas`) VALUES
(1, 'Tugas Algo', 'buat tugas yang sudah dibahas dikelas dan dikumpulkan minggu depan', 'reminder', '2016-02-17 06:06:59', 2, 2),
(2, 'UTS basdat', 'uts minggu depan di jam matakuliah, bahan dari pertemuan 1 sampai pertemuan 6, bersifat closed book', 'reminder', '2015-12-16 01:33:43', 4, 5),
(3, 'Tugas Algo', 'buat tugas yang sudah dibahas dikelas dan dikumpulkan minggu depan\r\n', 'reminder', '2016-02-17 06:07:13', 2, 1),
(4, 'UTS basdat', 'uts minggu depan di jam matakuliah, bahan dari pertemuan 1 sampai pertemuan 6, bersifat closed book', 'reminder', '2015-12-16 01:33:49', 4, 6),
(6, 'CRC', 'Mahasiswa IF,SI,TI dan DKV diwajibkan mengikuti seminar pada hari sabtu ini di Assembly Hall', 'General', '2016-02-17 06:17:35', 1, 7),
(7, 'Deadline Pembagian Kartu Ujian', 'Ditujukan kepada seluruh mahasiswa, pengambilan kartu ujian, paling akhri, besok pukul 15.00 WIB, terima kasih', 'Urgent', '2016-02-17 06:24:46', 1, 8),
(8, 'tes insert pengumuman', 'coba isi deskripsi', 'General', '2016-02-23 06:08:32', 1, 7),
(9, 'Tugas Prak SO', 'buat tugas yang sudah dibahas dikelas dan dikumpulkan minggu depan', 'General', '2016-03-01 04:44:35', 1, 7),
(10, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-01 05:54:37', 1, 7),
(11, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-01 05:57:49', 1, 7),
(12, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-01 06:00:32', 1, 1),
(13, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-01 06:01:16', 1, 1),
(14, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-01 06:01:29', 1, 1),
(15, 'test buat pengumuman', 'test buat', 'Reminder', '2016-03-01 06:03:49', 1, 2),
(16, 'test buat pengumuman 2', 'test buat 2', 'Reminder', '2016-03-01 06:04:32', 1, 2),
(17, 'Judul Pengumuman Jam Set 11', 'Saya lelah mengajar kalian', 'General', '2016-03-01 15:23:02', 1, 2),
(18, 'Pengumuman jam 10.35 malam', 'saya lelah mengajar', 'Urgent', '2016-03-01 15:25:31', 1, 1),
(19, 'Pengumuman jam 10.40', 'lalalala', 'Reminder', '2016-03-01 15:33:01', 1, 1),
(20, 'Test pengumuman dengan alert', 'halo alert please', 'General', '2016-03-01 15:40:50', 1, 2),
(21, 'Test pengumuman dengan alert2', 'halo alert please', 'General', '2016-03-01 15:41:22', 1, 2),
(22, 'test pengumuman dengan alert 3', 'dasd', 'Reminder', '2016-03-01 15:42:25', 1, 1),
(23, 'asd', 'asd', 'Reminder', '2016-03-01 15:43:57', 1, 1),
(24, 'baru', 'ini baru', 'Reminder', '2016-03-04 03:57:33', 1, 3),
(25, 'baru2', 'asda', 'General', '2016-03-04 04:38:29', 1, 1),
(26, 'Tugas Prak SO', 'bSDASDADulkan minggu depan', 'General', '2016-03-06 11:20:24', 1, 7),
(27, 'Kuliah pengganti', 'Sehubungan dengan libur di hari Rabu', 'General', '2016-03-06 12:19:49', 1, 7),
(28, 'Apink', 'Park Cho Rong is the leader', 'Urgent', '2016-03-06 12:47:08', 1, 7),
(29, 'Kuliah tambahan', 'Sehubungan dengan libur di hari Rabu, ada kuliah tambahan', 'General', '2016-03-06 12:49:23', 1, 7),
(30, 'test', 'halo guys', 'Reminder', '2016-03-10 14:42:35', 1, 2),
(31, 'test', 'halo', 'Reminder', '2016-03-10 14:43:31', 1, 2),
(32, 'dfff', 'ggg', 'Urgent', '2016-03-16 07:32:06', 1, 7),
(33, 'Apa kabar', 'Project KP', 'Urgent', '2016-03-26 13:55:44', 10, 7),
(34, 'Hello', 'World', 'Urgent', '2016-03-26 14:40:43', 10, 7),
(35, 'Coba Announce', 'Coba coba coba', 'General', '2016-03-27 12:40:18', 1, 7),
(36, 'baru', 'baru pengumuman', 'Reminder', '2016-03-29 15:56:32', 1, 2),
(37, 'asdasdasda', 'dasasdasdasdasda', 'General', '2016-03-29 15:57:21', 1, 2),
(38, 'baru', 'ini pengumuman baru', 'General', '2016-03-29 15:58:16', 1, 2),
(39, 'ini pengumuman', 'ini pengumuman', 'Reminder', '2016-03-29 16:02:35', 1, 2),
(40, 'ini pengumuman lagi', 'ini lagi pengumuman', 'General', '2016-03-29 16:03:33', 1, 2),
(41, 'asa', 'aaaaaa', 'Urgent', '2016-03-30 02:15:28', 1, 1),
(42, 'baru', 'ini pengumuman', 'Reminder', '2016-03-30 02:35:30', 1, 2),
(43, 'sdd', 'fcvv', 'Urgent', '2016-03-30 05:30:31', 10, 7);

-- --------------------------------------------------------

--
-- Table structure for table `seens`
--

CREATE TABLE IF NOT EXISTS `seens` (
  `user_id` int(10) NOT NULL,
  `peng_id` int(10) NOT NULL,
  KEY `user_id` (`user_id`,`peng_id`),
  KEY `peng_id` (`peng_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscribes`
--

CREATE TABLE IF NOT EXISTS `subscribes` (
  `subscribe_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  PRIMARY KEY (`subscribe_id`),
  KEY `user_id` (`user_id`,`id_kelas`),
  KEY `id_kelas` (`id_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subscribes`
--

INSERT INTO `subscribes` (`subscribe_id`, `user_id`, `id_kelas`) VALUES
(1, 10, 8),
(3, 11, 8),
(4, 12, 8),
(5, 13, 8),
(6, 14, 8);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `level` int(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `name`, `level`) VALUES
(1, 'admin', 'bukanadmin', 'admin aja dah', 0),
(2, 'dosen1', 'passdosen', 'dosen satu', 1),
(3, 'mahasiswa1', 'passmhs', 'mhs satu', 2),
(4, 'dosen2', 'passdosen', 'dosen dua', 1),
(5, 'mahasiswa2', 'passmhs', 'mhs dua', 2),
(6, 'dosen3', 'passdosen', 'dosen tiga', 1),
(8, '1111009', 'cobalagi', 'arnold', 0),
(9, '1111111', 'test', 'tes 1', 1),
(10, '1113031', 'apink12', 'Vincentius Yanuar', 2),
(11, '1113015', 'apink', 'Chandra Welim', 2),
(12, '1113015', 'apink', 'Chandra Welim', 2),
(13, '1113015', 'apink', 'Chandra Welim', 2),
(14, '1113004', 'baru', 'timo', 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `commentPeng_fk` FOREIGN KEY (`peng_id`) REFERENCES `pengumumans` (`peng_id`),
  ADD CONSTRAINT `commentUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `dosenmk`
--
ALTER TABLE `dosenmk`
  ADD CONSTRAINT `dosenMK_dosenFK` FOREIGN KEY (`dosen_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `dosenMK_mkFK` FOREIGN KEY (`mk_id`) REFERENCES `matakuliah` (`id_mk`);

--
-- Constraints for table `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelasMK_fk` FOREIGN KEY (`id_mk`) REFERENCES `matakuliah` (`id_mk`) ON DELETE CASCADE;

--
-- Constraints for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD CONSTRAINT `mkUser_fk` FOREIGN KEY (`dosen_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `pengumumans`
--
ALTER TABLE `pengumumans`
  ADD CONSTRAINT `pengKelas_fk` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `pengUsers_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `seens`
--
ALTER TABLE `seens`
  ADD CONSTRAINT `seensPeng_fk` FOREIGN KEY (`peng_id`) REFERENCES `pengumumans` (`peng_id`),
  ADD CONSTRAINT `seensUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `subscribes`
--
ALTER TABLE `subscribes`
  ADD CONSTRAINT `subsKelas_fk` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  ADD CONSTRAINT `subsUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
